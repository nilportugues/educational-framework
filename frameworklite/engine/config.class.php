<?php

class Configure
{
	private static $instance;

	private $config;

	function __construct()
	{ }

	public static function getInstance( $name )
	{
		if ( !isset( self::$instance[ $name ] ) )
		{
			self::$instance[ $name ] = new Configure;

			include ( PATH_CONFIGURE. $name . '.config.php' );

			self::$instance[ $name ]->config = $config;
		}

		return self::$instance[ $name ];
	}

	public function get( $name )
	{
		if( isset( $this->config[ $name ] ) )
		{
			return $this->config[ $name ];
		}
		else
		{
			throw new ConfigException( "Config parameter $name not exist<br>" );
		}
	}

	/**
	 * Return all the config variables.
	 *
	 */
	public function getConfig()
	{
		return $this->config;
	}

	public function getAll()
	{
		return $this->config;
	}

	static public function getClass( $type )
	{
		$config = Configure::getInstance( 'base' );
		$path	= $config->get( $type );

		if ( include_once $path )
		{
			$classname = $type;
			return new $classname;
		}
		else
		{
			throw new Exception ( "Class $type not exist" );
		}
	}
}

class ConfigException extends Exception
{
}

?>