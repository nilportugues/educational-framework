<?php
class Dispatcher
{
	function __construct()
	{
		try
		{
			$this->loadController();
		}
		catch( ConfigException $e )
		{
			if ( DEV_MODE )
			{
				echo $e->getMessage();
			}
			$module 	= Configure::getClass( 'ErrorError404Controller' );
			$module->execute();
			$module->show();
		}
		catch( Exception $e )
		{
			if ( DEV_MODE )
			{
				echo $e->getMessage();
			}
		}
	}

	public function loadController()
	{
		$config 	= Configure::getInstance( 'dispatcher' );

		$url_data = $this->getUrlData();

		$controller	= $config->get( $url_data['key_main_controller'] );

		$this->hasDebug();

		Filter::getInstance();

		$module 	= Configure::getClass( $controller );
		$module->setParams( $url_data );
		$module->execute();

		$module->show();

		// Get debug information.
		$module->buildDebug();

	}

	public function hasDebug()
	{
		if ( !SQL_DEBUG || !isset( $_GET['debug'] ) )
		{
			return false;
		}

		if ( $_GET['debug'] == 1 )
		{
			$_SESSION['debug'] = true;
		}
		elseif ( $_GET['debug'] == 0 )
		{
			$_SESSION['debug'] = false;
		}
	}

	public function getUrlData()
	{
		$current_url 	= $_SERVER['REQUEST_URI'];
		$current_domain	= $_SERVER['HTTP_HOST'];

		$requested_url = $current_domain . $current_url;

		// Split URL and classic GET params.
		$url_parts 	= explode( '?', $current_url );
		$clean_url 	= null;
		$get_params	= null;

		if ( isset( $url_parts[0] ) )
		{
			$clean_url 	= $url_parts[0];
		}
		if ( isset( $url_parts[1] ) )
		{
			$get_params	= $url_parts[1];
		}

		// Get the main controller key.
		$params 		= explode( '/', $clean_url );
		$num_params		= count( $params );

		$url_info['key_main_controller']		= 'default';

		if ( $num_params > 1 && !empty( $params[1] ) )
		{
			$url_info['key_main_controller'] = $params[1];

			for ( $i = 2; $i < $num_params; $i++ )
			{
				$url_info['url_arguments'][] = $params[$i];
			}
		}

		return $url_info;
	}
}

?>
