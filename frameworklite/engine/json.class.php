<?php

/**
 * This class is used to implements JSON Controllers. 
 *
 */
class Json extends Controller
{
	public function show() 
	{
		// Headers
		header('Content-type: application/json');
		// Print content
		echo json_encode( $this->data_response );
	}
}

?>